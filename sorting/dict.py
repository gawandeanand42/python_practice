#Dictionary Sorting
#This sorting usefull when all keys or values of same type. e.g string
d={'name':"Rudra",'designation':"Student","age":"24","OS":"Ubuntu"}

s_d=sorted(d.keys())#here only keys will be sorted

print(f"Sorted Dictionary:{s_d}")

s_d=sorted(d.values())#here only values will be sorted

print(f"Sorted Dictionary:{s_d}")
