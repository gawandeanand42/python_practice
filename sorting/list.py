li=[45,3,1,9,0,60,4]

s_li=sorted(li)

print(f"sorted numbers:{s_li}")
print(f"original list after sorted function:{li}")

s_li=li.sort()
print(f"sorted numbers:{s_li}")
print(f"original list after sort Method:{li}")


#Descending

s_li=sorted(li,reverse=True)

print(f"sorted numbers:{s_li}")
print(f"original list after sorted function:{li}")

