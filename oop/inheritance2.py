#advance 2
class Employee:
    raise_amt=1.04

    def __init__(self,first,last,pay):
        self.first=first
        self.last=last
        self.email=first+'.'+last+'@email.com'
        self.pay=pay
    
    def fullname(self):
        return f"{self.first} {self.last}"
    
    def apply_raise(self):
        self.pay=int(self.pay*self.raise_amt)


class Developer(Employee):
    raise_amt=1.10
    def __init__(self,first,last,pay,prog_lang):
        super().__init__(first,last,pay)
        self.prog_lang=prog_lang


dev_1=Developer("Rudra","Viv",50000,'Python')
emp_1=Employee("Akshay","pat",60000)

print(dev_1.prog_lang)