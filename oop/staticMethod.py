
class Employee:
    no_of_leaves = 8

    def __init__(self, aname, asalary, arole):
        self.name = aname
        self.salary = asalary
        self.role = arole

    def printdetails(self):
        return f"The Name is {self.name}. Salary is {self.salary} and role is {self.role}"

    @classmethod
    def change_leaves(cls, newleaves):
        cls.no_of_leaves = newleaves

    @classmethod
    def from_dash(cls, string):
        return cls(*string.split("-"))

    @staticmethod
    def printgood(string):
        print("This is good " + string)

harry = Employee("Rudra", 255, "Instructor")
rohan = Employee("Rohan", 455, "Student")
akshay = Employee.from_dash("Akshay-480-Student")

Employee.printgood("Rohan")




'''
Static methods:
-A static method in python must be created by decorating it with @staticmethod
 in order to let python now that the method should be static. The main characteristics 
 of a static method is that they can be called without instantiating the class. 
 This methods are self contained, meaning that they cannot access any other attribute 
 or call any other method within that class.
-You could use a static method when you have a class but you do not need an specific 
 instance in order to access that method. For example if you have a class called Math 
 and you have a method called factorial (calculates the factorial of a number). 
 You probably won’t need an specific instance to call that method so you could 
 use a static method.
'''