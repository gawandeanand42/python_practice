#Reading CSV File.
'''
By default Delimeter is comma.If csv file contain another delimeter than comma
then we must have to pass that delimiter explicitly in reader function to read it properly.
'''
import csv
with open("data.csv","r") as csv_file:
    csv_reader=csv.reader(csv_file)
   # print(csv_reader)#print object
    #next(csv_reader)#use if dont want to print fieldname like "Appeared".
    for line in csv_reader:
        #print(line[2])
        print(line)