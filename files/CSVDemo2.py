import csv
with open('SampleCSV.csv','r') as f:
    csv_reader=csv.reader(f)
    with open('new_SampleCSV.csv','w')as n_f:
        csv_writer=csv.writer(n_f,delimiter='-')# creating a dash separated file.
        
        for i in csv_reader:
            csv_writer.writerow(i)
    
print("contents written into file successfully")

'''
Here we are writing contents into file with '-'delimiter.If original file
contains '-' in text then that whole text is automatically put into " ".

'''