#Dict Writer
import csv
with open('SampleCSV.csv','r') as f:
    csv_reader=csv.DictReader(f)
    
    with open('Dict_SampleCSV.csv','w') as n_f:
        fieldname=['name','email','address'] # if we want to write only name and address then remove email.
        
        csv_writer=csv.DictWriter(n_f,fieldnames=fieldname,delimiter='\t')

        csv_writer.writeheader()#write fieldname as a first line
        for line in csv_reader:
            #del line['email']# if we want to write only name and address 
            csv_writer.writerow(line)
    
print("contents written into file successfully")