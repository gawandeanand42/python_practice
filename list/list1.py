#list Concept and operations

courses=['History','Math','Computer','Physics','chemistry','biology','geography',1,21,55,88]
print(courses)
print(courses[1])
print(courses[-1])
print(len(courses))
#print(courses[12])#Index Error

print(courses[0:2])#upto n-1
print(courses[2::2])#start from 2 to upto end by stepping by 2

courses.append('Art')
print(courses)#List are mutable

courses.insert(0,'English')#at 0th location

courses_2=['IT','Electronics']

courses.insert(0,courses_2)#add at beginning of list

courses_3=['Commerce','Accounts']
courses.extend(courses_3)#each item of courses_3 added to courses.If we use append then courses_3 will appended in courses

courses.remove('Math')
print(courses)


nums=[6,4,1,7,0,22,3,90]
nums.sort()
print(nums)
nums.sort(reverse=True)
print(nums)
'''
Note:- Here sort method changes original list.But if we want to print sorted list without modification
use sorted function instead of sort method. 
'''
print(sorted(nums))
print("After printing sorted list original list contents are: ",nums)

#Enumerate function: This function returns values and its index

for index,course in enumerate(courses,start=1):#here start=1 starts indexing from 1
    print(index,course)

#printing courses as a string

course_str=', '.join(courses[2:5])

print(course_str)