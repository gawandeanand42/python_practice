# def display(msg):
#     def display_msg():
#         print(f"displaying: {msg}")
    
#     return display_msg

# res=display("Welcome Rudra")
# res()

#Practical Example

def html_tag(tag):
    def wrap_text(msg):
        print(f"<{tag}>{msg}</{tag}>")
    return wrap_text

print_heading=html_tag('h1')#here print_heading becomes html_tag's inner function.
print_heading("This is Heading 1")

print_para=html_tag('p')
print_heading("This is paragraph")
