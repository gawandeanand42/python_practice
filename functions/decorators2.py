
# def decorator_function(original_function):
#     def wrapper_function():
#         print("\nWrapper\n")
#         return original_function()
#     return wrapper_function

# @decorator_function
# def display():
#     print("displaying")

# @decorator_function
# def display_info(name,age):
#     print(f"\nName:{name}\nAge:{age}\n")


# display_info("Rudra","24")

'''
Above code will give function argument error
to solve error we need args and kwargs

*args and **kwargs allow us to pass any number of 
argument and keyword arguments to function,  
'''

def decorator_function(original_function):
    def wrapper_function(*args,**kwargs):
        print("\nWrapper\n")
        return original_function(*args,**kwargs)
    return wrapper_function

@decorator_function
def display():
    print("displaying")

@decorator_function
def display_info(name,age):
    print(f"\nName:{name}\nAge:{age}\n")


display_info("Rudra","24")