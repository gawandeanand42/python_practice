#Returning multiple values

def calc(a,b):
    x=a+b
    y=a-b
    z=a*b
    u=a//b #rounded division
    return x,y,z,u #retrun as a Tuple

p,q=input("Enter Two numbers(must be separated by space): ").split(" ")

print(f'''\nAddition of {p} and {q} is : {calc(int(p),int(q))[0]}
          \nSubtraction of {p} and {q} is : {calc(int(p),int(q))[1]}
          \nMultiplication of {p} and {q} is : {calc(int(p),int(q))[2]}
          \nDivision(rounded) of {p} and {q} is : {calc(int(p),int(q))[3]}
    ''')