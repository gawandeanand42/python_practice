def square(x):
    return x*x

def cube(x):
    return x*x*x

def my_map(func,arg_list):
    result=[]
    for i in arg_list:
        result.append(func(i))#here fun is another name for calling function(cube/square).
    return result


res=my_map(cube,[1,2,3,4,5])#here put can square.

print(res)