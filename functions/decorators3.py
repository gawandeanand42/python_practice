# Class Decorators

class decorator_class(object):
    def __init__(self,original_function):
        self.original_function=original_function

    def __call__(self,*args, **kwargs):
        print("\nWrapper\n")
        return self.original_function(*args,**kwargs)

# def decorator_function(original_function):
#     def wrapper_function(*args,**kwargs):
#         print("\nWrapper\n")
#         return original_function(*args,**kwargs)
#     return wrapper_function

@decorator_class
def display():
    print("displaying")

@decorator_class
def display_info(name,age):
    print(f"\nName:{name}\nAge:{age}\n")


display_info("Rudra","24")