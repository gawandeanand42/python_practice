'''
when a file is being imported by some
other file it will it will print result
of imported file also so to avoid this we 
use __name__==__main__ in main file

Here multiply.py is main file which we will import
in example.py
'''

def mul(x,y):
    return x*y

if __name__=='__main__':
    print(mul(2,3))
    print("This is:",__name__)