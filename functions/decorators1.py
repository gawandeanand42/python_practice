
def decorator_function(original_function):
    def wrapper_function():
        print("\nWrapper execution\n")
        return original_function()
    return wrapper_function

@decorator_function
def display():
    print("\nHello this is decorator concept\n")

#decorator_display=decorator_function(display)

#decorator_display()
display() 

#to learn from scratch,fist uncomment commented code and comment to @decora.... and display().

'''
-Decorator is a function that takes another function as an argument as
some kind of functionality and then returns another function.
All of this without altering the source code of the original function 
that we pass as argument.

-It allows us to easily add functionality to our existing functions
by adding that functionality inside wrapper.

-So in above example we are added print in wrapper function without modifying display function.

- We can replace "decorator_display=decorator_function(display)"
  by putting @decorator_function before display function definition.
  so @decorator_function is equivalent to "decorator_display=decorator_function(display)" statement.

'''