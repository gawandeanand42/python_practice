'''
A Closure is a function object that 
remembers values in enclosing scopes
even if they are not present in memory
'''
def outer_func():
    message="Hi"
    
    def inner_func():
        print(message)
    
    return inner_func()  #here we are execuring function first and returning it's result.
                         #be careful with ()

outer_func()

'''
In simple terms a closure is an inner function
that remembers and has access to variables in local 
scope in which it was created even after the
outer function has finished execution.

When and why to use closures:
1)Closures can avoid the use of global values and provides some form of data hiding. 
It can also provide an object oriented solution to the problem.

2)When there are few methods (one method in most cases) to be implemented in a class,
 closures can provide an alternate and more elegant solutions. But when the number
  of attributes and methods get larger, better implement a class.

3)Here is a simple example where a closure might be more preferable than 
defining a class and making objects. But the preference is all yours

'''