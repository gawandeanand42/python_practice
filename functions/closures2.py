#Arithmetic program using closure.

def function(f):
    def fun_args(*args):
        print(f(*args))
    return fun_args

def add(x,y):
    return f"\n {x}+{y}:{x+y}"
def sub(x,y):
    return f" {x}-{y}:{x-y}\n"

    
add_fun=function(add)
sub_fun=function(sub)

add_fun(5,5)
sub_fun(8,6)